import React, {useState } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Nouser from './pages/Nouser';
import Listing from './pages/List';
import SearchResult from './pages/SeachResult'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

function App() {

  const [open, setOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const handleOpen = () => {
    setFormData(selectedUser || {name: '', address: '', hobby: ''});
    setOpen(true);
  };
  const handleClose = () => {
    setSelectedUser(null);
    setOpen(false);
  };
  const [users, setUsers] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    address: '',
    hobby: ''
  })
  const handleChange = (e) => {
    let dat = {...formData}
    dat[e.target.name] = e.target.value
    setFormData(dat)
  }
  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
    const results = users.filter(user =>
      user.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setSearchResults(results);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let data = [...users];
    if (selectedUser) {
      const index = data.findIndex(user => user.name === selectedUser.name);
      data[index] = formData;
      setSelectedUser(null);
    } else {
      data.push(formData);
    }
    setUsers(data);
    setFormData({
      name: '',
      address: '',
      hobby: ''
    });
    handleClose();
  }
  console.log(selectedUser)
  const handleEdit = (user) => {
    setSelectedUser(user);
    handleOpen();
  }
  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          MyApp
        </Typography>
        <Button 
        onClick={handleOpen}
        variant="contained"
        style={{
          backgroundColor:'#00c2cb',
          borderRadius: '13px'
        }}
        >
          Add User
        </Button>
      </Toolbar>
    </AppBar>
    <TextField sx={{
    marginTop: "15px",
    textAlign: 'center'
    }} 
    id="outlined-basic" 
    label="Search" 
    variant="outlined" 
    onChange={handleSearch}
    />
    {users.length === 0 ? <Nouser/> : searchResults.length === 0 || searchTerm === '' ? <Listing users={users} handleEdit={handleEdit} handleOpen={handleOpen}/> : <SearchResult searchResults={searchResults} handleEdit={handleEdit} handleOpen={handleOpen}/>}
    <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h4" component="h2" style={{textAlign: 'center'}}>
          {selectedUser ? "Edit User" : 'Add User'}
          </Typography>
          <Typography id="modal-modal-title" variant="h6" component="h2" >
           Name
          </Typography>
          <TextField 
          id="outlined-basic"  
          variant="outlined" 
          margin='normal' 
          name='name'
          fullWidth 
          value={ formData.name}
          onChange={handleChange}
          />
          <Typography id="modal-modal-title" variant="h6" component="h2" >
           Adress
          </Typography>
          <TextField 
          id="outlined-basic"  
          variant="outlined" 
          margin='normal' 
          name='address'
          fullWidth  
          value={formData.address}
          onChange={handleChange}
          />
          <Typography id="modal-modal-title" variant="h6" component="h2" >
           Hobby
          </Typography>
          <TextField 
          id="outlined-basic"  
          variant="outlined" 
          margin='normal' 
          fullWidth 
          name='hobby'
          value={formData.hobby}
          onChange={handleChange}
          />
          <br/>
          <Button 
          onClick={handleSubmit}
          variant="outlined">Save</Button>
        </Box>
      </Modal>
  </Box>
  );
}

export default App;
