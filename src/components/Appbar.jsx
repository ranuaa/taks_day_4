import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

const Appbar = ({handleOpen}) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            My App
          </Typography>
          <Button 
          onClick={() => handleOpen}
          variant='contained' 
          style={{
            backgroundColor: "#00c2cb",
            borderRadius: "13px"
          }}
          >
            Add User
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Appbar;
