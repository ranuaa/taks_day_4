import React from "react";
import { Typography } from "@mui/material";
const Nouser = () => {
 return (
    <div style={{
        width: '100%',
        height: '80vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Typography variant='h1' color='#f7b531'>
          <strong>0</strong>
        </Typography>
        <Typography variant='h1' color='#ff5a5a'>
          <em><strong>USER</strong></em>
        </Typography>
      </div>  
 );
};

export default Nouser;
