import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import { Button, ListItemButton } from '@mui/material';


const Listing = ({searchResults}) => {
  return (
    <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
    {searchResults.map((user) => {
        return(
        <div style={{
            width: '100%',
            border: '1px solid black'
        }}>
        <ListItem key={user.name}>
        <div style={{width: '50%'}}>
        <ListItemText primary={user.name} secondary={user.address} />
        </div>
        <div style={{width: '50%', display:'flex', flexDirection: 'column', justifyContent:'center', alignItems: 'center' }}>
        <ListItemText primary={user.hobby} />
        <ListItemButton>
          <Button 
          variant='contained' 
          style={{
            backgroundColor: "#00c2cb",
            borderRadius: "13px"
          }}
          >
            Edit
          </Button>
        </ListItemButton>
        </div>
        </ListItem>
        </div>
        )
    })}
  </List>
  )
};

export default Listing;
